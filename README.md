# kindle clipper

#### 介绍
将kindle剪贴板充分利用起来，支持各种常用的功能，初步计划提供web版、ios版和android版。

#### 开发计划

1. web版，使用Koa+Mysql+Vue，预计上线时间2019/07/01
2. ios及android版，使用Flutter开发，预计上线时间2019/09/01

#### 功能列表

1. 用户注册、登陆
2. My Clippings.txt文件导入管理
3. 文件解析
4. 图书管理
5. 标注管理
6. 标注分享
7. 待续 
#### 安装教程

#### 使用说明

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
